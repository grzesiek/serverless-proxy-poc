GO_LDFLAGS := -extldflags "-static" -w -s

.PHONY: build
build: clean
	GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags '$(GO_LDFLAGS)' -o serverless_proxy_poc

.PHONY: clean
clean:
	rm -f serverless_proxy_poc
