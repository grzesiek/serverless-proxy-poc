package main

import (
	"flag"

	"github.com/sirupsen/logrus"

	"gitlab.com/grzesiek/serverless_proxy_poc/proxy"
)

var domain, cluster string

func init() {
	flag.StringVar(&domain, "domain", "", "Your function FQDN domain")
	flag.StringVar(&cluster, "cluster", "", "Your cluster IP address")

	flag.Parse()

	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
}

func main() {
	logrus.Info("staring serverless proxy")

	reverseProxy := proxy.NewServerlessReverseProxy(domain, cluster)

	if err := reverseProxy.Listen(); err != nil {
		logrus.WithError(err).Fatal("error occured")
	}
}
