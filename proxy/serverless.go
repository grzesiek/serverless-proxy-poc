package proxy

import (
	"net/http"
	"net/http/httputil"
	"os"
)

const (
	frontTLSKey  = "ssl/front_key.pem"
	frontTLSCert = "ssl/front_cert.pem"
)

type ServerlessProxy struct {
	*httputil.ReverseProxy
}

func NewServerlessReverseProxy(domain, address string) *ServerlessProxy {
	proxy := httputil.ReverseProxy{
		Director:       proxyDirector(domain),
		ModifyResponse: proxyModifyResponse(),
		Transport:      NewProxyTransport(domain, address),
	}

	return &ServerlessProxy{ReverseProxy: &proxy}
}

func proxyDirector(domain string) func(request *http.Request) {
	return func(request *http.Request) {
		request.Host = domain
		request.URL.Host = domain
		request.URL.Scheme = "https"
		request.Header.Set("User-Agent", "ReverseProxy PoC")
	}
}

func proxyModifyResponse() func(response *http.Response) error {
	return func(response *http.Response) error {
		return nil
	}
}

func (proxy *ServerlessProxy) Listen() error {
	if _, err := os.Stat(frontTLSKey); os.IsNotExist(err) {
		return http.ListenAndServe(":8080", proxy)
	} else {
		return http.ListenAndServeTLS(":443", frontTLSCert, frontTLSKey, proxy)
	}
}
