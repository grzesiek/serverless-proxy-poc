package proxy

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	clientKey  = "ssl/key.pem"
	clientCert = "ssl/cert.pem"
)

type proxyTransport struct {
	transport *http.Transport
}

func NewProxyTransport(domain, cluster string) http.RoundTripper {
	dialer := net.Dialer{
		Timeout:   60 * time.Second,
		KeepAlive: 60 * time.Second,
	}

	dialContext := func(ctx context.Context, network, address string) (net.Conn, error) {
		logrus.WithField("address", address).Info("dialing address")

		if address == domain+":443" {
			address = cluster + ":443"
		}

		return dialer.DialContext(ctx, network, address)
	}

	return &proxyTransport{
		transport: &http.Transport{
			DialContext:         dialContext,
			TLSHandshakeTimeout: 5 * time.Second,
			TLSClientConfig:     newTLSConfig(),
		},
	}
}

func (proxy *proxyTransport) RoundTrip(request *http.Request) (*http.Response, error) {
	startTime := time.Now()
	response, err := proxy.transport.RoundTrip(request)

	logrus.
		WithField("duration", time.Now().Sub(startTime)).
		WithField("request", request.URL).
		Info("proxied")

	return response, err
}

func newTLSConfig() *tls.Config {
	cert, err := tls.LoadX509KeyPair(clientCert, clientKey)
	if err != nil {
		logrus.WithError(err).Fatal("can not load certificates")
	}

	caCert, err := ioutil.ReadFile(clientCert)
	if err != nil {
		logrus.WithError(err).Fatal("can not read CA cert")
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
		ServerName:   "poc.serverless.gitlab.io",
	}

	return tlsConfig
}
