# Serverless Reverse Proxy PoC

# How To

## Configure mutual TLS in a Knative cluster

Perform following action in the `ssl/` directory:

```
mkdir ssl/
cd ssl/
```

### Generate self-signed certificates for mutual TLS

Generate self-signed certificates

```
$ openssl req -newkey rsa:2048  -new -nodes -x509 -days 365 \
  -out cert.pem -keyout key.pem
```

Enter the domain that the proxy will work for. In our case we do have
hardcoded: `poc.serverless.gitlab.io`. In real life it should contain a cluster
/ project identifier.

## Deploy certificates

Deploy certificates to a Knative cluster.

```
$ kubectl create -n istio-system secret tls istio-ingressgateway-certs --key key.pem --cert cert.pem
$ kubectl create -n istio-system secret generic istio-ingressgateway-ca-certs --from-file=cert.pem
```

Note that you should not change the names of the secrets here.

Edit your `knative-ingress-gateway` to configure mutual TLS:

```
$ kubectl edit gateway knative-ingress-gateway --namespace knative-serving
```

Replace the TLS configuration section from PASSTHROUGH to:

```
tls:
  mode: MUTUAL
  privateKey: /etc/istio/ingressgateway-certs/tls.key
  serverCertificate: /etc/istio/ingressgateway-certs/tls.crt
  caCertificates: /etc/istio/ingressgateway-ca-certs/cert.pem
```

Note that `cert.pem` needs to match deployed certificate nam in `--from-file=cert.pem`

## Verify mTLS

Check if mutual TLS works, for example:

```
curl --resolve functions-echo.serverless-proxy-poc-14317698-development.knative.example.com:443:35.202.210.123 \
  --cacert cert.pem --cert cert.pem --key key.pem \
  https://functions-echo.serverless-proxy-poc-14317698-development.knative.example.com
```

## Configure front certificates

Generate them with Let's Encrypt or buy them, and place them in
`ssl/front_key.pem` and `ssl/front_cert.pem`.

You can use a domain of your choice here.

# Deploy and run PoC Serverless Proxy

```
$ go run main.go --domain [your function domain] --cluster [ip address of the cluster]
```
